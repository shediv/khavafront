'use strict';

/* Controllers */

angular.module('angularRestfulAuth')
    .controller('HomeCtrl', ['$rootScope', '$scope', '$location', '$localStorage', 'Main', function($rootScope, $scope, $location, $localStorage, Main) {

        //Login User
        $scope.signin = function() {
            var formData = {
                user : {
                        username: $scope.username,
                        password: $scope.password
                }        
            }

            Main.signin(formData, function(res) {
                if (res.type == false) {
                    alert(res.data)    
                } else {                    
                    $rootScope.loggedin = true;
                    $rootScope.data = res.data;
                    //$localStorage.token = res.token;                                        
                    $location.path('/dashboard')                        
                }
            }, function() {
                $rootScope.error = 'Failed to signin';
            })
        };

        //Create User
        $scope.signup = function() {            
            //If verification code entered
            if($scope.vcode != undefined){
                var formData = {
                    user : {
                        mobile: $scope.mobile,
                        vcode: parseInt($scope.vcode),
                        userId : $scope.newuser
                    }
                }

                Main.verify(formData, function(response) {
                    if (response.type == false) {
                        alert(response.data)
                    } else {
                        //$localStorage.token = res.data.token;
                        //window.location = "/"
                        console.log(response);
                        $scope.sresponse = response;
                        $rootScope.userId = $scope.newuser;
                        $rootScope.vcode = parseInt($scope.vcode);
                        $rootScope.vcodeSucess = true;
                        $rootScope.buildings = [
                            {
                                _id : "1",
                                "name" : "building 1"
                            },
                            {
                                _id : "2",
                                "name" : "building 2"
                            },
                            {
                                _id : "3",
                                "name" : "building 3"
                            }
                        ],
                        $location.path('/details') 
                    }
                }, function() {
                    $rootScope.error = 'Failed to signup';
                    $scope.error = 'Wrong verification Code';
                })
            }else
            {

                var formData = {
                    user : {
                        mobile: $scope.mobile
                    }
                }

                Main.signup(formData, function(response) {
                    if (response.type == false) {
                        alert(response.data)
                    } else {
                        //$localStorage.token = res.data.token;
                        //window.location = "/"
                        console.log(response);
                        $scope.newuser = response.userId._id; 

                    }
                }, function() {
                    $rootScope.error = 'Failed to signup';
                })
            }
        };

        $scope.register = function() {                                
                var formData = {
                    user : {
                        name: $scope.name,
                        email: $scope.email,
                        password: $scope.password,
                        userId: $rootScope.userId,
                        vcode: $rootScope.vcode,
                    }
                }

                Main.register(formData, function(response) {
                    if (response.type == false) {
                        alert(response.data)
                    } else {
                        //$localStorage.token = res.data.token;
                        //window.location = "/"
                        console.log(response);
                        $scope.registered = true; 

                    }
                }, function() {
                    $rootScope.error = 'Failed to Save details';
                })            
        };

        $scope.me = function() {
            Main.current(function(res) {
                $scope.myDetails = res;
            }, function() {
                $rootScope.error = 'Failed to fetch details';
            })
        };

        $scope.logout = function() {
            Main.logout(function() {
                window.location = "/"
            }, function() {
                alert("Failed to logout!");
            });
        };
        $scope.token = $localStorage.token;
    }])

.controller('MeCtrl', ['$rootScope', '$scope', '$location', 'Main', function($rootScope, $scope, $location, Main) {

        Main.me(function(res) {
            $scope.myDetails = res;
        }, function() {
            $rootScope.error = 'Failed to fetch details';
        })
}]);
