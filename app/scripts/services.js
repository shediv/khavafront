'use strict';

angular.module('angularRestfulAuth')
    .factory('Main', ['$http', '$localStorage', function($http, $localStorage){
        var baseUrl = "http://localhost:3001";
        function changeUser(user) {
            angular.extend(currentUser, user);
        }

        function urlBase64Decode(str) {
            var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
                case 0:
                    break;
                case 2:
                    output += '==';
                    break;
                case 3:
                    output += '=';
                    break;
                default:
                    throw 'Illegal base64url string!';
            }
            return window.atob(output);
        }

        function getUserFromToken() {
            var token = $localStorage.token;
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
                user = JSON.parse(urlBase64Decode(encoded));
            }
            return user;
        }

        var currentUser = getUserFromToken();

        return {
            //Create User
            signup: function(data, success, error) {
                $http.post(baseUrl + '/user/signup', data).
                    success(success).error(error)
            },
            register: function(data, success, error) {
                $http.post(baseUrl + '/user/register', data).
                    success(success).error(error)
            },
            verify: function(data, success, error) {
                $http.post(baseUrl + '/user/verify', data).
                    success(success).error(error)
            },
            signin: function(data, success, error) {
                $http.post(baseUrl + '/user/authenticate', data).success(success).error(error)
            },
            me: function(success, error) {
                var config = {
                    headers : {
                        'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NmQ3ZGU3NWY4NmUzZDMxMzM5NmQxMmEiLCJlbWFpbCI6ImhhcmlzaEB0aGVtZWRpYWFudC5jb20iLCJwYXNzd29yZCI6ImJhMmMyNzBjYmE2NTUxYWIwMTBlNThjN2ZlNDdlYzAxIiwiaXNBY3RpdmUiOjEsImRhdGVPZkpvaW4iOiIyMDE2LTAzLTAzVDA2OjQ5OjI1LjcwNFoiLCJfX3YiOjAsImlhdCI6MTQ1Njk4ODM1NCwiZXhwIjoxNDU3NjY4NzU0fQ.xr6iDJERa5mZ0qror2LT7w6nhi8kJTjN9-6mDWRgrDM'
                    }
                }
                $http.get(baseUrl + '/user/current', config).success(success).error(error)
            },
            logout: function(success) {
                changeUser({});
                delete $localStorage.token;
                success();
            }
        };
    }
]);